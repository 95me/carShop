# carShop
[![fork](https://gitee.com/95me/carShop/badge/fork.svg?theme=gray)](https://gitee.com/95me/carShop/members)

#### 介绍
二手车网站前端页面，仅供学习交流，下载二次开发请联系说明，麻烦点个star

#### 软件架构
	1. 框架：jquery,bootstrap
	2. 插件：layer 


#### 安装教程

1. hbuilder
2. 建议在服务器模式下运行

#### 使用说明

1. 项目导入到hbuilder中运行
2. 仅供学习和交流之用，使用之前请与本人取得联系，说明用途并得到本人同意后。
3. 源码可以进行修改和传播，但是修改后源码必须合并到原版本(本版本为最初版本，任何版本的变动都必须合并到此版本之后才允许发布)
4. 最初版本拥有者（本人）可以删除相关合并到本版本的任意代码
5. 不可将本应用修改后私自用作他用，需和本人取得联系，本应用最终解释权归本人和代码贡献者所有。
6. 任何使用者下载和修改代码视为同意以上协议，且本协议具有法律约束。
7. 若要进行代码修改，请联系本人申请成为项目开发者。


#### 参与贡献

1. 何伟
2. 1807158216@qq.com
3. 电话（微信）：17795179212

#### 示例图片


#### 首页
![Image text](https://gitee.com/95me/carShop/raw/master/demoimg/1.jpg)


#### 卖车页面
![Image text](https://gitee.com/95me/carShop/raw/master/demoimg/2.jpg)


#### 买车页面
![Image text](https://gitee.com/95me/carShop/raw/master/demoimg/3.jpg)


#### 论坛页面
![Image text](https://gitee.com/95me/carShop/raw/master/demoimg/4.jpg)


#### 商城页面
![Image text](https://gitee.com/95me/carShop/raw/master/demoimg/5.jpg)

#### 评估页面
![Image text](https://gitee.com/95me/carShop/raw/master/demoimg/6.jpg)


#### 支付中心
![Image text](https://gitee.com/95me/carShop/raw/master/demoimg/7.jpg)


#### 个人中心
![Image text](https://gitee.com/95me/carShop/raw/master/demoimg/8.jpg)